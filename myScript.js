const Nom = 'Philippe Fleury'
const DateNaissance = new Date(2000, 6, 31);
const Sexe = 'H'
const SituationParticuliere = 'non'
let maxAlcoolSemaine
let maxAlcoolJour
let Recommandation

let d = new Date();
d.setFullYear(2020);
let AnneeAge = Number(d.getFullYear() - DateNaissance.getFullYear());
let n = d.toLocaleDateString();
let h = d.toLocaleTimeString();

let nbJours = Number(prompt("Combien de jours voulez-vous analyser?"));

let journeesAlcool = []
let journeesAlcool2 = []
let journeesAlcool3 = []
for (i = 1; i < nbJours + 1; i++) {
    let nbConsommationAlcool = Number(prompt("Entrez le nombre de consommation d'alcool"));
    journeesAlcool.push(nbConsommationAlcool);
    journeesAlcool2.push(nbConsommationAlcool);
    journeesAlcool3.push(nbConsommationAlcool);
};

let sommeHebdo = journeesAlcool.reduce(sum);
function sum(total, value, index, array) {
    return total + value;
};

let moyenne = sommeHebdo / nbJours;

if (SituationParticuliere === 'Oui' && "oui") {
    maxAlcoolSemaine = 0
    maxAlcoolJour = 0
}
else if (Sexe === 'H' && 'h' && "M" && 'm') {
    maxAlcoolSemaine = 15
    maxAlcoolJour = 3
}
else {
    maxAlcoolSemaine = 10
    maxAlcoolJour = 2
};

let max = journeesAlcool.sort(function (a, b) { return b - a });

for (i = 0; i < journeesAlcool2.length; i++) {
    if (journeesAlcool2[i] >= 1) {
            journeesAlcool2.splice(i, 1);
            i--;
    }
};

let nbJoursZero = journeesAlcool2.length

for (i = 0; i < journeesAlcool3.length; i++) {
    if (journeesAlcool3[i] < maxAlcoolJour) {
        journeesAlcool3.splice(i, 1);
        i--;
    }
};

let nbJoursExcédant = journeesAlcool3.length

ratioExcedant = nbJoursExcédant * 100 / nbJours;
ratioZeroAlcool = nbJoursZero * 100 / nbJours;

if (sommeHebdo > maxAlcoolSemaine) {
	Recommandation = "Vous ne respectez pas les recommandations."
}
else if (max[0] > maxAlcoolJour) {
	Recommandation = "Vous ne respectez pas les recommandations."
}
else {
	Recommandation ="Vous respectez les recommandations."
};

console.log(n + ' à ' + h);
console.log(" Fleury, Philippe");
console.log(" Âge : " + AnneeAge + " an(s)");
console.log(" Alcool : " + sommeHebdo);
console.log(" Moyenne par jour : " + moyenne.toFixed(2));
console.log(" Consommation sur une semaine : " + sommeHebdo + "  Recommandation : " + maxAlcoolSemaine);
console.log(" Maximum en une journée : " + journeesAlcool[0] + "  Recommandation : " + maxAlcoolJour);
console.log(" Ratio de journées excédants : " + parseFloat(ratioExcedant).toFixed(2) + " % ");
console.log(" Ratio de journées sans alcool : " + parseFloat(ratioZeroAlcool).toFixed(2) + " % ");
console.log(Recommandation)

/* J'ai fait les deux n'etant pas sure du quelle vous preferiez */

document.getElementById("date").innerHTML = n + ' à ' + h;
document.getElementById("nom").innerHTML = "Fleury, Philippe";
document.getElementById("age").innerHTML = ("Âge : " + AnneeAge + " an(s)");
document.getElementById("alcool").innerHTML = ("Alcool : " + sommeHebdo);
document.getElementById("moyenne").innerHTML = ("Moyenne par jour : " + moyenne.toFixed(2));
document.getElementById("Recommandationsemaine").innerHTML = ("Consommation sur une semaine : " + sommeHebdo + "  Recommandation : " + maxAlcoolSemaine);
document.getElementById("Recommandationjour").innerHTML = ("Maximum en une journée : " + max[0] + "  Recommandation : " + maxAlcoolJour);
document.getElementById("ratex").innerHTML = ("Ratio de journées excédants : " + ratioExcedant.toFixed(2) + " %");
document.getElementById("ratsal").innerHTML = ("Ratio de journées sans alcool : " + ratioZeroAlcool.toFixed(2) + " %");
document.getElementById("Recommandations").innerHTML = (Recommandation);
